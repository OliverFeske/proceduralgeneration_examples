﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class FractalPlant : MonoBehaviour
{
	[SerializeField] private string ruleForX = "F+<[[<X]<-X]<-F[<-FX]<+X";
	//[SerializeField] private string ruleForX = "F+[[X]-X]-F[-FX]+X";
	[SerializeField] private string ruleForF = "FF";
	//[SerializeField] private string ruleForF = "FF";

	[SerializeField] private GameObject twigPrefab;
	[SerializeField] private Transform result;
	[SerializeField] private string axiom = "X";
	[SerializeField] private float angle = 25f;
	[SerializeField] private float angle2Min = 0f;
	[SerializeField] private float angle2Max = 25f;
	[SerializeField] private float length = 1f;
	[SerializeField] private int iterations = 4;

	private Dictionary<char, string> rules = new Dictionary<char, string>();
	private Stack<SavedValue> savedValues = new Stack<SavedValue>();
	private string sentence;
	private float angle2;

	void Start()
	{
		result.gameObject.SetActive(false);
		sentence = axiom;
		rules.Add('X', ruleForX);
		rules.Add('F', ruleForF);
		for (int i = 0; i < iterations; i++)
		{
			GenerateSentence();
		}
		GenerateMesh();
	}

	void GenerateSentence()
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < sentence.Length; i++)
		{
			char curChar = sentence[i];

			if (rules.ContainsKey(curChar))
				sb.Append(rules[curChar]);
			else
				sb.Append(curChar);
		}
		sentence = sb.ToString();
	}
	void GenerateMesh()
	{
		for (int i = 0; i < sentence.Length; i++)
		{
			char curChar = sentence[i];

			if (curChar == 'F')
			{
				var twig = Instantiate(twigPrefab, transform.position, transform.rotation, result.transform);
				// move forward
				transform.Translate(Vector3.up * length);
				Combine(result);
				Destroy(twig);

			}
			else if (curChar == '+')
			{
				transform.Rotate(Vector3.forward * angle);
			}
			else if (curChar == '-')
			{
				transform.Rotate(Vector3.back * angle);
			}
			else if(curChar == '<')
			{
				angle2 = Random.Range(angle2Min, angle2Max);
				transform.Rotate(Vector3.right * angle2, Space.World);
			}
			else if(curChar == '>')
			{
				angle2 = Random.Range(angle2Min, angle2Max);
				transform.Rotate(Vector3.left * angle2, Space.World);
			}
			else if (curChar == '[')
			{
				var savedItem = new SavedValue(transform.position, transform.rotation);
				savedValues.Push(savedItem);
			}
			else if (curChar == ']')
			{
				var saveditem = savedValues.Pop();
				transform.position = saveditem.pos;
				transform.rotation = saveditem.rot;
			}
		}
	}

	void Combine(Transform result)
	{
		MeshFilter[] meshFilters = result.GetComponentsInChildren<MeshFilter>();
		CombineInstance[] combineInstances = new CombineInstance[meshFilters.Length];
		for (int i = 0; i < meshFilters.Length; i++)
		{
			combineInstances[i].mesh = meshFilters[i].sharedMesh;
			combineInstances[i].transform = meshFilters[i].transform.localToWorldMatrix;
			meshFilters[i].gameObject.SetActive(false);
		}
		result.GetComponent<MeshFilter>().mesh = new Mesh();
		result.GetComponent<MeshFilter>().mesh.CombineMeshes(combineInstances);
		result.gameObject.SetActive(true);
	}
}

public struct SavedValue
{
	public SavedValue(Vector3 position, Quaternion rotation)
	{
		pos = position;
		rot = rotation;
	}

	public Vector3 pos;
	public Quaternion rot;
}