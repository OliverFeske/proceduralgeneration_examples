﻿public class Roof : Shape
{
	private ShapeData myData;
	public override ShapeData Data =>  myData;

	public Roof() : base()
	{
		shapeType = ShapeType.Roof;
	}
	public Roof(ShapeData data) : base(data)
	{
		myData = new ShapeData(data);
		myData.Heigth = 0.5f;
		shapeType = ShapeType.Roof;
	}

	public override void Expand()
	{
	}
}