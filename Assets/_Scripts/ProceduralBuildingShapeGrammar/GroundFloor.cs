﻿using UnityEngine;

public class GroundFloor : Shape
{
	private ShapeData myData;
	public override ShapeData Data => myData;

	public GroundFloor() : base()
	{
		shapeType = ShapeType.Ground;
	}
	public GroundFloor(ShapeData data) : base(data)
	{
		myData = new ShapeData(data);
		myData.Heigth = Random.Range(1, 3);

		shapeType = ShapeType.Ground;
	}

	public override void Expand()
	{
		nextShape = new Floor(base.Data, 0);
		nextShape.Expand();
	}
}