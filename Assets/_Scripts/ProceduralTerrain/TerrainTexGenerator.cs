﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TerrainTexGenerator : MonoBehaviour
{
	[SerializeField] private KeyCode kc_PaintTextures = KeyCode.P;
	[SerializeField] private List<TextureAttributes> textureAttributes = new List<TextureAttributes>();
	[SerializeField] private Terrain terrain;
	private TerrainData data;
	// 3D-array, for x, y and textureLayer
	private float[,,] alphaMap;
	private int idxDefaultTex;
	private float maxHeight;

	void Start()
	{
		data = terrain.terrainData;
		maxHeight = GetMaxHeight(data, data.heightmapResolution);
		alphaMap = new float[data.alphamapWidth, data.alphamapHeight, data.alphamapLayers];
		for (int i = 0; i < textureAttributes.Count; i++)
		{
			if (textureAttributes[i].IsDefaultTexture)
			{
				idxDefaultTex = textureAttributes[i].Idx;
			}
		}
	}
	void Update()
	{
		if (Input.GetKeyDown(kc_PaintTextures))
		{
			PaintTextures();
		}
	}

	float GetMaxHeight(TerrainData data, int hmWidth)
	{
		float max = 0f;
		for (int y = 0; y < hmWidth; y++)
		{
			for (int x = 0; x < hmWidth; x++)
			{
				if (data.GetHeight(x, y) > max)
				{
					max = data.GetHeight(x, y);
				}
			}
		}
		return max;
	}

	void PaintTextures()
	{
		maxHeight = GetMaxHeight(data, data.heightmapResolution);
		for (int y = 0; y < data.alphamapHeight; y++)
		{
			for (int x = 0; x < data.alphamapWidth; x++)
			{
				float x01 = (float)x / data.alphamapWidth;
				float y01 = (float)y / data.alphamapHeight;
				float height = data.GetHeight(Mathf.RoundToInt(y01 * data.heightmapResolution), Mathf.RoundToInt(x01 * data.heightmapResolution));
				// normalize to actual height (instead of max possible height)
				float heightNormalized = height / maxHeight;
				float steepness = data.GetSteepness(y01, x01);
				float steepnessNormalized = steepness / 90f;
				// clear all layers
				for (int i = 0; i < data.alphamapLayers; i++)
				{
					alphaMap[x, y, i] = 0f;
				}
				float[] weights = new float[data.alphamapLayers];
				// go through all textures and set alphaMap value to 1 if in range
				for (int i = 0; i < textureAttributes.Count; i++)
				{
					if (heightNormalized >= textureAttributes[i].MinAltitude &&
						heightNormalized <= textureAttributes[i].MaxAltitude &&
						steepnessNormalized >= textureAttributes[i].MinSteepness &&
						steepnessNormalized <= textureAttributes[i].MaxSteepness)
					{
						weights[textureAttributes[i].Idx] = 1f;
					}
				}
				// normalizing factor z, e.g. if 4 textures have value 1, then z is 4
				float z = weights.Sum();
				// if sum is approximately 0 then no texture is set => use default texture
				if (Mathf.Approximately(z, 0f))
				{
					weights[idxDefaultTex] = 1f;
				}
				// go through all layers, normalize value (e.g. 1/4 = 0.25f)
				// write to alphaMap[,,]
				for (int i = 0; i < data.alphamapLayers; i++)
				{
					weights[i] /= z;
					alphaMap[x, y, i] = weights[i];
				}
			}
		}
		// pass the array to terrainData starting at (0, 0)
		data.SetAlphamaps(0, 0, alphaMap);
	}
}

[System.Serializable]
public class TextureAttributes
{
	public string Name;
	public int Idx;
	public bool IsDefaultTexture = false;
	[Range(0f, 1f)] public float MinSteepness;
	[Range(0f, 1f)] public float MaxSteepness;
	[Range(0f, 1f)] public float MinAltitude;
	[Range(0f, 1f)] public float MaxAltitude;
}