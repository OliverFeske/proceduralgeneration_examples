﻿using System;
using System.Collections.Generic;

public class MarkovNameGenerator
{
	private Dictionary<string, List<char>> chains = new Dictionary<string, List<char>>();
	private List<string> samples = new List<string>();
	private List<string> used = new List<string>();
	private Random random = new Random();
	// tells how many letters are put in a 'package' to add together
	private int order;
	// minimum length of the name
	private int minLength;

	// constructor 
	// IEnumerable for using foreach, recognizes each line
	// takes string[], List<string>...
	public MarkovNameGenerator(IEnumerable<string> _sampleNames, int _order, int _minLength)
	{
		// clamp parameters
		if (_order < 1) { _order = 1; }
		if (_minLength < 1) { _minLength = 1; }

		order = _order;
		minLength = _minLength;

		// splitting and formatting 
		// for each line in the namelist do sth
		foreach (string line in _sampleNames)
		{
			// split at ',' in case of multiple names in the same line
			string[] tokens = line.Split(',');
			// foreach word found do sth
			foreach (string word in tokens)
			{
				// deletes all spaces in front and at the back of the string
				// make all char capital
				string upper = word.Trim().ToUpper();

				// if the name is shorter than the order + 1 then do not because then it would be a full name ? 
				if (upper.Length < order + 1) { continue; }

				// finally add the formatted string into an array of workable strings
				samples.Add(upper);
			}
		}

		// build chains
		// for each word in the samples[] do sth
		foreach (string word in samples)
		{
			// add a letter until the word is shortened down by the order
			// this is made because of the last part of the word has the size of order
			for (int letter = 0; letter < word.Length - order; letter++)
			{
				// creates a new string of the word starting with the letter and takes the next amount(order) of letters
				string token = word.Substring(letter, order);
				List<char> entries = null;

				// if the List already contains the same group of letters just take the entry
				if (chains.ContainsKey(token))
				{
					entries = chains[token];
				}
				// if it does not contain this group of letters, add them
				else
				{
					entries = new List<char>();
					chains[token] = entries;
				}
				// add the word(sample in Upper) to the entries[] 
				entries.Add(word[letter + order]);
			}
		}
	}

	public string NextName
	{
		get
		{
			// start with an empty string
			string s = "";

			// as long as used contains this name already or the length of the name is shorter than the min length
			do
			{
				// generate a random int
				int n = random.Next(samples.Count);
				// with this int check fot the length of a name in the list and say that this is the length of our new word
				int nameLength = samples[n].Length;
				// generates a string with the length of order at a random position of the sample string
				s = samples[n].Substring(random.Next(0, nameLength - order), order);
				// while the length of the s string is smaller than the name length
				while (s.Length < nameLength)
				{
					// creates a new token of the length order of s, that in each while iteration gets a new character
					string token = s.Substring(s.Length - order, order);
					// gets a random character out of the token
					char c = GetLetter(token);

					// there is no error, add c to the end of s
					if (c != '?')
						s += c;
					else
						break;
				}
				// if s coontains  a space ( e.g. if there are multiple names )
				if (s.Contains(" "))
				{
					// create an array of names
					string[] tokens = s.Split(' ');
					// start with an empty string
					s = "";
					// for each token do sth
					for (int t = 0; t < tokens.Length; t++)
					{
						// if the token is empty just go to the next one
						// else let the first letter be capital and the rest lower
						if (tokens[t] == "")
							continue;
						else
							tokens[t] = tokens[t].Substring(0, 1) + tokens[t].Substring(1).ToLower();

						// if s is not empty add a space between the names
						if (s != "")
							s += " ";

						// add the finished name to s
						s += tokens[t];
					}
				}
				// first letter upper case, remaining letters lower case
				else
				{
					s = s.Substring(0, 1) + s.Substring(1).ToLower();
				}

			} while (used.Contains(s) || s.Length < minLength);

			// in case of new or long enough name, add it to used
			used.Add(s);
			return s;
		}
	}

	// reset the used names
	public void Reset()
	{
		used.Clear();
	}

	// returns a random letter of this token
	char GetLetter(string token)
	{
		// if no entry -> no random successor possible
		if (!chains.ContainsKey(token)) { return '?'; }
			
		List<char> letters = chains[token];
		int n = random.Next(letters.Count);
		return letters[n];
	}
}
